terraform {
  required_version = ">= 0.13.1"

  required_providers {
    aws    = ">= 3.60"
    random = ">= 2.0"
  }
}

resource "aws_s3_bucket" "b" {
  bucket = "davidmtokobucket"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}